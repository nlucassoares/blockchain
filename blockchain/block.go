package blockchain

import (
	"crypto/sha512"
	"encoding/binary"
	"encoding/hex"
)

type Block struct {
	// previous block hash
	PreviousHash string

	// current target
	Target uint64

	// hash
	Hash string

	// timestamp
	Timestamp uint64

	// data
	Data *Data

	// nonce
	Nonce uint64
}

// calculate block hash
func ( block *Block ) CalculateHash( ) ( [ ]byte, error ) {
	hash := sha512.New( )
	hash.Write( [ ]byte( block.PreviousHash ) )
	hash.Write( Uint64ToByteArray( block.Timestamp ) )
	hash.Write( Uint64ToByteArray( block.Target ) )
	if merkleHash, err := block.Data.CalculateMerkleTreeHash( ); err != nil {
		return nil, err
	} else {
		hash.Write( merkleHash )
	}
	hash.Write( Uint64ToByteArray( block.Nonce ) )
	return hash.Sum( nil ), nil
}

// mine block
func ( block *Block ) Mine( ) {
	for {
		if !block.IsHashCorrect( ) {
			block.Nonce++
		} else {
			hash, _ := block.CalculateHash( )
			block.Hash = hex.EncodeToString( hash )
			break
		}
	}
}

// is hash correct?
func ( block *Block ) IsHashCorrect( ) bool {
	if hash, err := block.CalculateHash( ); err == nil {
		if binary.BigEndian.Uint64( hash[ : 8 ] ) < block.Target {
			return true
		}
	}
	return false
}
