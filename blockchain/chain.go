package blockchain

type Chain struct {
	// block list
	Block [ ]*Block

	// difficulty
	Target uint64
}
