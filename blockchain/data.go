package blockchain

import (
	"crypto/sha512"
	"fmt"
)

type Data struct {
	// word list
	Word [ ]string
}

// calculate word hash
func ( data *Data ) CalculateWordHash( index int ) [ ]byte {
	hash := sha512.New( )
	hash.Write( [ ]byte( data.Word[ index ] ) )
	doubleHash := sha512.New( )
	doubleHash.Write( hash.Sum( nil ) )
	return doubleHash.Sum( nil )
}

// calculate data merkle tree hash
func ( data *Data ) CalculateMerkleTreeHash( ) ( [ ]byte, error ) {
	// check words count
	if len( data.Word ) <= 0 {
		return nil, fmt.Errorf( "bad word count" )
	}

	// merkle tree leaves
	leaf := make( [ ][ ]byte,
		0,
		1 )

	// calculate hashes for each word and save as leaves
	for i := range data.Word {
		leaf = append( leaf,
			data.CalculateWordHash( i ) )
	}

	// apply merkle tree
	if len( leaf ) > 1 {
		for length := len( leaf ); length > 1; length /= 2 {
			j := 0
			for i := 0; i < length; i += 2 {
				if i + 1 >= length {
					leaf[ j ] = leaf[ i ]
				} else {
					hash := sha512.New( )
					hash.Write( leaf[ i ] )
					hash.Write( leaf[ i + 1 ] )
					doubleHash := sha512.New( )
					doubleHash.Write( hash.Sum( nil ) )
					leaf[ j ] = doubleHash.Sum( nil )
				}
				j++
			}
			if length % 2 != 0 &&
				j > 1 {
				leaf[ j ] = leaf[ j - 1 ]
				length++
			}
		}
	}
	return leaf[ 0 ], nil
}
