Blockchain
==========

Introduction
------------

The goal here is to produce a working blockchain.

The protocol is taken from bitcoin model.

The objective is to have a transactions pool made of simple words. A block can accept multiple words.

Technical details
-----------------

Where bitcoin uses sha256, i'm using here sha512.

Target is value first 64 bits of block hash must be under. Lower target means higher difficulty.

Author
------

SOARES Lucas <lucas.soares.npro@gmail.com>

https://gitlab.com/NLucasSoares/blockchain.git
